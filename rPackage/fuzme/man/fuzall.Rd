\name{fuzall}
\docType{methods}
\alias{fuzall}
\title{Allocation of memberships to clusters derived from fuzzy k means}
\description{Computes the cluster memberships of data given data to pre-defined fuzzy-k centroids.}

\usage{fuzall(data,phi,centroid,distype,W)}
\arguments{
 \item{data}{\code{data.frame}; Data that needs to be allocated to pre-defined clusters. Only continuous data can be used. Variables of the data must be the same as for the variables of the centroids.}
 \item{phi}{\code{numeric}; Fuzzy exponent value. 1 equates to a hard clustering. Greater than 1 introduces increasing fuzziness of clustering.}
 \item{centroid}{\code{matrix}; Cluster centroids}
  \item{distype}{\code{numeric}; Distance metric to be used for clustering. 1 = Euclidean, 2= Diagonal, 3 = Mahalanobis.}
  \item{W}{\code{numeric}; Distance normalization matrix 1 = diagonal 1 matrix  2= diagonal variance matrix 3= variance-covariance matrix.}
}

\value{This function returns a \code{list} which contains 3 elements:
\itemize{
   \item \code{membership}. A \code{matrix} of memberships of each data to each cluster.
   \item \code{distance}. A \code{matrix} of distances between each data and cluster centroid.
   \item \code{objectiveF}. \code{numeric}; The minimized objective function value estimated from clustering algorithm. 
   }}

\note{This function was translated from Matlab scripts originally developed by Budiman Minasny and implemented in the \href{http://sydney.edu.au/agriculture/pal/software/fuzme.shtml}{Fuzme software} which is distributed by the University of Sydney Precision Agriculture Laboratory.}

\author{Brendan Malone}

\references{
\itemize{
\item Minasny, B., McBratney, A.B., (2002) \href{http://sydney.edu.au/agriculture/pal/software/fuzme.shtml}{FuzME version 3.0}. Australian Center for Precision Agriculture, The University of Sydney, Australia. 
}}


\examples{
#data
data(iris)
data<- iris[,1:4] #data to be clustered

nclass<- 5                # number of class
phi<- 1.5                      # fuzzy exponent >1
maxiter<- 300                # maximum iterations
toldif<- 0.01            # convergence criterion
distype<- 3             #   distance type:        1 = euclidean, 2 = diagonal, 3 = mahalanobis
scatter<- 0.2                # scatter around initial membership
ntry<- 1               # number of trial to choose an optimal solution
verbose=0

#####################################
#run fuzzy kmeans
test1<- runFuzme(nclass= nclass,data= data,phi= phi,maxiter= maxiter,distype= distype,toldif= toldif,scatter= scatter,ntry= ntry,verbose= 0)

#inputs for fuzzy allocation
data<- data
centroid<- test1$centroid
phi<- 1.5
W<- test1$distNormMat
distype<- 3

#run fuzzy allocation
datAll<- fuzall(data= data,phi= phi,centroid= centroid,distype= 3,W= W)
str(datAll)
}
\keyword{methods}
