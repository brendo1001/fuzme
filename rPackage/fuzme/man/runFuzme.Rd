\name{runFuzme}
\docType{methods}
\alias{runFuzme}
\title{Fuzzy k-means}
\description{Computes a fuzzy clustering of data into k clusters given a fuzzy exponent value.}

\usage{runFuzme(nclass,data,phi,maxiter,distype,toldif,scatter,ntry,verbose)}
\arguments{
  \item{nclass}{\code{numeric}; The number of classes one wishes to derive from input data set.} 
  \item{data}{\code{data.frame}; Data that needs to be clustered. Only continuous data can be used. No missing data.}     
  \item{phi}{\code{numeric}; Fuzzy exponent value. 1 equates to a hard clustering. Greater than 1 introduces increasing fuzziness of clustering.}
  \item{maxiter}{\code{numeric}; Maximum allowed iterations of algorithm to perform regardless of whether it has converged.}
  \item{distype}{\code{numeric}; Distance metric to be used for clustering. 1 = Euclidean, 2= Diagonal, 3 = Mahalanobis.}
  \item{toldif}{\code{numeric}; Fuzzy algorithm tolerance factor for assessing convergence. A number between 0 and 1. Closer to zero equates to low tolerance (convergence takes longer to achieve).}
  \item{scatter}{\code{numeric}; For determining initial memberships to centroids. A random scatter. Higher the number = more scatter. Number between 0 and 1.}
  \item{ntry}{\code{numeric}; Number of times algorithm is run in order to seek an optimal clustering of data.}
  \item{verbose}{\code{numeric}; 1 to display algorithm processing. 0 to have no display }
}

\value{This function returns a \code{list} which contains 5 element:
\describe{
   \item{\code{membership}}{A \code{matrix} of memberships of each data to each cluster.}
   \item{\code{centroid}}{A \code{matrix} of data centroids.}
   \item{\code{distance}}{A \code{matrix} of distances between each data and cluster centroid.}  
   \item{\code{distNormMat}}{A square \code{matrix} for normalization of distance calculations. 1. For Euclidean distances this is a diagonal matrix with ones on the diagonal and zeros elsewhere. 2. For diagonal distance this is a diagonal matrix with variances and the diagonal and zeros elsewhere. 3. For Mahalanobis distance this is the variance-covariance matrix of the data. } 
   \item{\code{objectiveFUnction}}{\code{numeric} The minimized objective function value from the clustering algorithm.} 
   }}

\note{This function was translated from Matlab scripts originally developed by Budiman Minasny and implemented in the \href{http://sydney.edu.au/agriculture/pal/software/fuzme.shtml}{Fuzme software} which is distributed by the University of Sydney Precision Agriculture Laboratory.}

\author{Brendan Malone}

\references{
\itemize{
\item Minasny, B., McBratney, A.B., (2002) \href{http://sydney.edu.au/agriculture/pal/software/fuzme.shtml}{FuzME version 3.0}. Australian Center for Precision Agriculture, The University of Sydney, Australia.
\item Bezdek, J. C., (1981) Pattern Recognition with Fuzzy Objective Function Algorithms (Advanced Applications in Pattern Recognition). Plenum Press.
}}


\examples{
# runFuzme inputs

data(iris)
data<- iris[,1:4] #data to be clustered

nclass<- 5  # number of class
phi<- 1.5   # fuzzy exponent >1
maxiter<- 300   # maximum iterations
toldif<- 0.01   # convergence criterion
distype<- 3     #   distance type:        1 = euclidean, 2 = diagonal, 3 = mahalanobis
scatter<- 0.2   # scatter around initial membership
ntry<- 5        # number of trial to choose an optimal solution
verbose=0     # I dont want to print outputs

####################
#run function
test1<- runFuzme(nclass= nclass,data= data,phi= phi,maxiter= maxiter,distype= distype,toldif= toldif,scatter= scatter,ntry= ntry,verbose= 0)
#####################

}
\keyword{methods}
