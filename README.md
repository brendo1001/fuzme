# README #

fuzme R package

This packages does kmeans clustering and kmeans with extragrades clustering.

A software of this package is available from:
http://sydney.edu.au/agriculture/pal/software/fuzme.shtml


For installation in R the following commands work:

Make sure the devtools package is installed first

// install.packages("devtools")

library(devtools)

install_bitbucket("brendo1001/fuzme/rPackage/fuzme")