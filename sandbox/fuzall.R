function [U,dist,obj] = fuzall(data,phi,centroid,distype,W)
# fuzzy k means allocation
# calculate membership U & distance matrix, given centroid & phi
# [U,dist,obj] = fuzall(data,phi,centroid,distype,W)
#
# input
#   data        = data matrix           data(ndata,ndim)
#   phi         = fuzzy exponent        >1
#   centroid    = centroid              centroid(nclass, ndim)
#   distype     = distance type:        1 = euclidean, 2 = diagonal, 3 = mahalanobis
#   W           = distance norm matrix  output from fuzme
#
# output:
  #   U           = membership matrix
#   dist        = distance matrix       dist(ndata,nclass)
#   obj         = objective function
#

#inputs
data<- data
dist<- test1$distance
centroid<- test1$centroid
phi<- 1.5
W<- test1$distNormMat
distype<- 3


#run fuzzy allocation
datAll<- fuzall(data,phi,centroid,distype,W)
datAll$membership
test1$membership


##Fuzzy allocation function
fuzall<- function(data,phi,centroid,distype,W){
nclass<- dim(centroid)[1]     # number of class
ndata<- dim(data)[1]         # number of data 
ndim<-  dim(data)[2]         # number of dimension
U<- matrix(0, nrow=ndata, ncol=nclass)


# calculate distance of data to centroid
if(distype==1) # euclidean distance
{dist<- matrix(0, nrow= ndata, ncol=nclass)
 for (euc1 in 1:ndata){
   for (euc2 in 1:nclass){
     dist[euc1,euc2]<- sqrt(sum((as.matrix(data[euc1,])- centroid[euc2, ])^2))
   }}}
if(distype == 2 | distype == 3 ) #mahalinobis or diagonal
{dist<- matrix(0, nrow= ndata, ncol=nclass)
 for (euc1 in 1:ndata){
   for (euc2 in 1:nclass){
     dist[euc1,euc2]<- sqrt(mahalanobis(x= as.matrix(data[euc1,]), center= centroid[euc2, ], cov= W))
   }}}

#calculate new membership matrix
tmp<-  dist^(-2/(phi-1))      
t1<- as.matrix(rowSums(tmp))
t2<- matrix(rep(t1,nclass), nrow=ndata,ncol=nclass)
U<-  tmp / t2
       
#calculate objective function
uphi <-  U^phi   
o1<- (dist^2) * uphi 
obj<- sum(rowSums(o1)) 

retval<- list(U,dist,obj)
names(retval)<- c("membership", "distance", "objectiveF")
return(retval)}
    
