# fuzzy validity
#calculate cluster validity value
# Developed: July 2015
# Developer: Brendan Malone
# Notes: This script is the result of converting freely available Matlab script. Matlab script authored by Budiman Minasny 
# Notes: Takes outputs of either runFuzme and fkme




# Input:
#   U           = membership matrix     U(ndata,nclass)
#   W           = distance norm matrix  W(ndim,ndim)
#   centroid    = cluster centre        centroid(nclass,ndim)
#   dist        = distance matrix       dist(ndata,nclass)
#   nclass      = no. class
#   phi         = fuzzy exponent        >1

# Output:
  #   fpi = fuzzy performance index
  #   nce = normalised classification entropy
  #   S   = cluster separation 
  #   dJdphi = derivative of objective function over phi dJ/dphi


#inputs
dist<- tester$distance
centroid<- tester$centroid
nclass<- 3
phi<- 1.5
W<- tester$distNormMat
U<- tester$membership[,1:nclass]

#######RUN FUNCTION
fvalidity(U = U, dist = dist, centroid = centroid, nclass = nclass,phi = phi, W= W )

 
fvalidity<- function(U, dist, centroid, nclass,phi, W ){
valid.mat<- matrix(NA, nrow=1, ncol=4)
ndata<-  dim(U)[1]      # number of data

# fuzzy performance index
F = (1/ndata) %*% sum(colSums(U*U))
f = (nclass*F-1)/(nclass-1)
fpi = 1-f
valid.mat[1,1]<- fpi

# mpe
H = -1/ndata*sum(colSums(U*log(U)))
nce = H/log(nclass)
valid.mat[1,2]<- nce

# S
disc<- matrix(0, nrow=nclass, ncol=nclass)
for (cen in 1:nclass){
disc[cen,] = mahalanobis(centroid, centroid[cen,], W)}
eps<- .Machine$double.eps
disc<- disc + (diag(nclass) *(1/eps))
dmin<- min(disc)
ud<- (U * U) * (dist * dist) 
j2<- sum(colSums(ud))
S<- j2 / (ndata*dmin)
valid.mat[1,3]<- S

# dJ/dphi
uphi<-  U^phi
d1<- (dist^2) *log(U) * uphi
dJdphi<-  sum(rowSums(d1))
valid.mat[1,4]<- dJdphi
valid.mat<- as.data.frame(valid.mat)
names(valid.mat)<- c("fpi", "mpe", "S", "dJ/dphi")
return(valid.mat)}



