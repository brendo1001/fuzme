#run fuzme

#sample command to run Fuzme
data(iris)
data<- iris[,1:4] #data to be clustered

nclass<- 3                  # number of class
ndata<-  dim(data)[1]         # number of data 
phi<- 1.5                      # fuzzy exponent >1
maxiter<- 300                # maximum iterations
toldif<- 0.01            # convergence criterion
distype<- 3             #   distance type:        1 = euclidean, 2 = diagonal, 3 = mahalanobis
scatter<- 0.2                # scatter around initial membership
ntry<- 10                 # number of trial to choose an optimal solution
verbose=1

#####################################
#run function
test1<- runFuzme(nclass,data,phi,maxiter,distype,toldif,scatter,ntry,verbose=0)
#####################

#run Fuzme function
runFuzme<- function(nclass,data,phi,maxiter,distype,toldif,scatter,ntry,verbose){
#start function
ndim<-   dim(data)[2]          # number of dimension
ftry<- matrix(0, nrow=ntry, ncol= 1)
fmin<- 1.e30
cbest<- matrix(1, nrow=nclass, ncol=ndim)

for (i in 1:ntry){
  print(paste("Try no = ", i, sep=""))
  Uinit<-  initmember(scatter=scatter,nclass=nclass,ndata=ndata) #inital membership matrix
  test1<- fuzme(nclass=nclass,data=data,Uinit=Uinit,phi=phi,maxiter=maxiter,distype=distype,toldif= toldif, verbose=verbose) #run fuzme
  obj<- test1$objectiveFUnction 
  if (obj < fmin) {fmin<- obj
                   cbest<- test1$centroid
                   U<- test1$membership
                   dist<- test1$distance
                   W<- test1$distNormMat}
  ftry[i,1]<- obj}
retval<- list(U, cbest,dist, W, ftry )
names(retval)<- c("membership", "centroid", "distance", "distNormMat", "objectiveFUnction")
return(retval)}



  